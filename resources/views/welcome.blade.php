@extends('layouts.app')

@section('content')
<div class="container" style="padding-top: 10px;">
    <div class="row">
        <div class="col-md-12">
          @foreach($entries as $key => $entry)
            <div class="col-sm-6">
              @include('snippets.blogshort', ['title' => $entry->title, 'time' => $entry->updated_at, 'id' => $entry->id, 'can_publish' => false])
            </div>
          @endforeach
          <?php echo $entries->links(); ?>
        </div>
    </div>
</div>
@include('snippets.blogdetail')
@endsection