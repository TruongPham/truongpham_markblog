@extends('layouts.app')

@section('content')
<div class="container">
  <ul  class="nav nav-pills">
    @if ($entry_status == 1)
    <li class="active">
    @else
    <li>
    @endif
      <a  href="{{route('manage', 1)}}">Public</a>
    @if ($entry_status == 2)
    <li class="active">
    @else
    <li>
    @endif
    <a href="{{route('manage', 2)}}">Wait for Verifying</a>
    </li>
  </ul>
</div>
<div class="container" style="padding-top: 10px;">
    <div class="row">
        <div class="col-md-12">
          @foreach($entries as $key => $entry)
            <div class="col-sm-6">
              @include('snippets.blogshort', ['title' => $entry->title, 'time' => $entry->updated_at, 'id' => $entry->id, 'can_publish' => ($entry_status == 2)])
            </div>
          @endforeach
          <?php echo $entries->links(); ?>
        </div>
    </div>
</div>
@include('snippets.blogdetail')
@endsection