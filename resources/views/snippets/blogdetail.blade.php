
<div class="modal fade" id="blog_detail_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <div>
          <h3 class="modal-title">Loading entry content....</h3>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close" style='margin-top:-30px;'>
            <span aria-hidden="true"><i class="fa fa-times" aria-hidden="true"></i></span>
          </button>
        </div>
        <p class="modal-entry-date">2017/01/10</p>
        <span class="modal-entry-author">Truong pham</span>
      </div>
      <div class="modal-body">
        <div class="loader-wrapper">
          <div>
            <div class="loader"></div>
          </div>
        </div>
        <div class='modal-entry-content-wrapper'>
          <p class='modal-entry-content'></p>
        </div>
      </div>
      <div class="modal-footer">
        <a class="btn btn-secondary" data-dismiss="modal">Close</a>
      </div>
    </div>
  </div>
</div>