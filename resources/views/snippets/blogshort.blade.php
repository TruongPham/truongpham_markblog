
<div class="card" style="background-color: #fff;border: 1px solid rgba(0,0,0,.125); margin-bottom: 5px; margin-top: 5px">
  <div class="card-block" style="-webkit-box-flex: 1;-webkit-flex: 1 1 auto;-ms-flex: 1 1 auto;flex: 1 1 auto;padding: 1.25rem;">
    <h3 class="card-title">{{$title}}</h3>
    <p class="card-text">{{$time}}</p>
    <a href="javascript:openEntryDetail({{$id}})" class="btn btn-outline-info">View Detail</a>
    @if($can_publish)<a href="{{route('publish', $id)}}" class="btn btn-outline-info">Publish</a>@endif
  </div>
</div>



<script type="text/javascript">

  function openEntryDetail(id) {
    if($('#blog_detail_modal')) {
      var entryUrl = '/api/blog/' + id;
      entryLoading($('#blog_detail_modal'));
      $('#blog_detail_modal').modal();
      $.ajax({
            type: 'GET',
            url: entryUrl,
            success: function (data) {
                if(data.success) {
                  entryLoaded($('#blog_detail_modal'), data.entry);
                } else {
                  entryError($('#blog_detail_modal'), data.message);
                }

            },
            error: function (data) {
                entryError($('#blog_detail_modal'), "Fail to load resource from JD's Blog, please try again!");
            }
        });
    }
  }

  function entryLoading(modal) {
    $(modal).find('.modal-title').text('Loading entry content...');
    $(modal).find('.modal-entry-content').text('');
    $(modal).find('.modal-entry-date').text('');
    $(modal).find('.modal-entry-author').text('');
    $(modal).find('.loader-wrapper').show();
  }

  function entryError(modal, message) {
    $(modal).find('.modal-title').text('Fail to load entry');
    $(modal).find('.modal-entry-date').text('');
    $(modal).find('.modal-entry-author').text('');
    $(modal).find('.modal-entry-content').text(message);
    $(modal).find('.loader-wrapper').hide();
  }

  function entryLoaded(modal, entry) {
    $(modal).find('.modal-title').text(entry.title);
    $(modal).find('.modal-entry-content').html(entry.content);
    $(modal).find('.modal-entry-date').text(entry.updated_at);
    $(modal).find('.modal-entry-author').text(entry.user.name);
    $(modal).find('.loader-wrapper').hide();
  }
</script>