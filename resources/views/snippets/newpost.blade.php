
<div class="modal fade" id="new_post_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">New post to JD's blog</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" style='margin-top:-22px;'>
          <span aria-hidden="true"><i class="fa fa-times" aria-hidden="true"></i></span>
        </button>
      </div>
      <div class="modal-body">
        <form>
          <div class="form-group">
            <label for="recipient-name" class="form-control-label blog_post_title_label">Title (0/200):</label>
            <input type="text" class="form-control blog_post_title">
          </div>
          <div class="form-group">
            <label for="message-text" class="form-control-label">Content:</label>
            <textarea class="form-control blog_post_content" rows="10" style="resize: none;"></textarea>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <span class="modal-warning pull-left text-danger"></span>
        <span class="modal-info pull-left text-info"></span>
        <a type="button" class="btn btn-secondary" data-dismiss="modal">Close</a>
        <a href="javascript:save()" id="popup_blog_save" class="btn btn-primary">Post <i class="fa fa-paper-plane-o" aria-hidden="true"></i></a>
      </div>
    </div>
  </div>
</div>

<script>

function save() {
  var title = $('#new_post_modal').find('.blog_post_title').val();
  var content = $('#new_post_modal').find('.blog_post_content').val();
  if(!title || title == '') {
     onPostError('Title can not be empty');
      return;
    }
    if(!content || content == '') {
      onPostError('Content can not be empty');
      return;
    }
  converter = new showdown.Converter(),
  content      = converter.makeHtml(content);
  postEntry({title: title, content: content});
}


</script>