<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Blo Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'notExists' => 'Blog entry not found',
    'duplicatedTitle' => 'Already have entry with this title',
    'entryTitleEmtpy' => 'Entry must have title',
    'entryTitleBreakLimit' => "Entry's title has maximum 200 characters",
    'entryContentEmtpy' => 'Entry must have content',
    'entryCreataSuccess' => 'Entry create successfully, Admin is checking!',
    'userNotLoggedIn' => 'You have to logged in to done this action',
];
