<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Auth;
use App\Models\Blog;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

       $entries = Blog::where('status', '=', Config::get('constants.BLOG_STATUS.VERIFIED'))->paginate(20);

       return view('welcome', ['entries' => $entries]);
    }

    public function manage($entry_status = 0) {
        if(!Auth::user()->authorizeAdminRole()) {
            return redirect()->route('home');
        }
        if(!$entry_status) {
            $entry_status = Config::get('constants.BLOG_STATUS.VERIFIED');
        }
        $entries = Blog::where('status', '=', $entry_status)->paginate(20);

        return view('manage', ['entries' => $entries, 'entry_status' => $entry_status]);
    }
}
