<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use App\Models\Blog;

class BlogController extends Controller
{
    public function __construct()
    {
    }
    //
    public function detail($entry_id) {

        $entry = Blog::find($entry_id);
        $data['success'] = true;
        $data['message'] = '';
        if(!$entry) {
            $data['success'] = false;
            $data['message'] = __('blog.notExists');
        } else {
            $entry->user;
            $data['entry'] = $entry;
        }
        return response()->json($data);
    }

    public function create() {
        if(!Auth::check()) {
            return response()->json([
                'success' => false,
                'message' => __('blog.userNotLoggedIn')
            ]);
        }
        $params = request()->all(); 
        $title = $params['title'];
        $content = $params['content'];

        if(!$title || empty($title)) {
            return response()->json([
                'success' => false,
                'message' => __('blog.entryTitleEmtpy')
            ]);
        } else if(strlen($title) > Config::get('constants.ENTRY_TITLE_LENGTH')) {
            return response()->json([
                'success' => false,
                'message' => __('blog.entryTitleBreakLimit')
            ]);
        }

        if((!$content || empty($content))) {
            return response()->json([
                'success' => false,
                'message' => __('blog.entryContentEmtpy')
            ]);
        }

        $existed = Blog::where('title', $title)->first();
        if($existed) {
            return response()->json([
                'success' => false,
                'message' => __('blog.duplicatedTitle')
            ]);
        }

        $entry = new Blog;
        $entry->title = $title;
        $entry->content = $content;
        $entry->author_id = Auth::id();
        if(Auth::user()->authorizeAdminRole()) {
            $entry->status = Config::get('constants.BLOG_STATUS.VERIFIED');
        } else {
            $entry->status = Config::get('constants.BLOG_STATUS.PENDING');
        }
        $entry->save();
        return response()->json([
                'success' => true,
                'message' => __('blog.entryCreataSuccess')
            ]);
    }

    public function publish($entry_id) {
         if(Auth::user()->authorizeAdminRole()) {
            $entry = Blog::find($entry_id);

            if($entry) {
                $entry->status = Config::get('constants.BLOG_STATUS.VERIFIED');
                $entry->save();
            }
        }
        return redirect()->route('manage', Config::get('constants.BLOG_STATUS.VERIFIED'));
    }

}
