<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');

Route::get('/manage/{entry_status?}', 'HomeController@manage')->name('manage')->middleware('auth');
Route::get('/publish/entry/{entry_id}', 'BlogController@publish')->name('publish')->middleware('auth');

Route::post('/create/entry', 'BlogController@create');
