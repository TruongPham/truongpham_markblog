// $('#new_post_modal').on('show.bs.modal', function (event) {
//   // var button = $(event.relatedTarget) // Button that triggered the modal
//   // var recipient = button.data('whatever') // Extract info from data-* attributes
//   // // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
//   // // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
//   // var modal = $(this)
//   // modal.find('.modal-title').text('New message' + recipient)
//   // modal.find('.modal-body input').val(recipient)
// })
$( ".blog_post_title" ).keypress(function() {
      var title = $(this).val();
      var currentLength = title ? title.length : 0;
      if(title && title.length > 200) {
        title = title.substr(0, 200);
        $(this).val(title);
        return false;
      }
      $('.blog_post_title_label').text("Title ("+currentLength+"/200)");
});

function onPostError(message) {
  $('#new_post_modal').find('.modal-warning').text(message);
  $('#new_post_modal').find('.modal-info').text('');
}

function onPostInfo(message) {
  $('#new_post_modal').find('.modal-warning').text('');
  $('#new_post_modal').find('.modal-info').text(message);
}

function disablePostAction() {
  $('#popup_blog_save_as_draft').attr('href', '#');
  $('#popup_blog_save').attr('href', '#');
}

function enablePostAction() {
  $('#popup_blog_save_as_draft').attr('href', 'javascript:save(1)');
  $('#popup_blog_save').attr('href', 'javascript:save(0)');
}

function onPostEntryResult() {
  enablePostAction();
  $('#new_post_modal').modal('hide');
  window.location.reload();
}

function postEntry(params) {
  disablePostAction();
  var _token = $('meta[name="csrf-token"]').attr('content');
  params._token = _token;
  var postUrl = '/create/entry';
  $('#new_post_modal').find('.modal-warning').text('');
  onPostInfo('Posting...');
  $.ajax({
      type: 'POST',
      data: params,
      dataType: 'json',
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
          'CSRF_TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      url: postUrl,
      success: function (data) {
          if(data.success) {
            onPostInfo(data.message);
          } else {
            onPostError(data.message);
          }
          onPostEntryResult()          
      },
      error: function (data) {
          onPostError("Fail to create entry, please try again!");
          enablePostAction();
      }
  });
}