Welcome to JD's Blog


To setup this project please follow these steps :

I. Requirement
PHP : > 5.6
APACHE
MySQL
composer

II. Configuration
1. Install dependencies
    - Go to root folder of project
    - Run: composer update
    - Give write permission for folder : 
        + storage/
        + bootstrap/cache/
    - In .env : give your domain URL in entry "APP_URL"
2. Database
    - Set database name, host, username, password: 
        Please edit file .env , section DB
    - Import file database/scripts/init_database.sql into database

3. Apache config
    There are two way, you can choose one what you prefer
    * use .htaccess
        - Copy project folder into you machine webroot folder
        - Access by url : http://your_domain.abc/your_project_folder
    * use apache httpd.conf  (virtual host)
        - Insert this config into your httpd config file:
        <VirtualHost *:80>
            ServerName "your_server_name"
            DocumentRoot "/path/to/your/project"
            <Directory "/path/to/your/project">
            Options -MultiViews +FollowSymLinks
            RewriteEngine On

            # Handle Front Controller...
            RewriteCond %{REQUEST_FILENAME} !-d
            RewriteCond %{REQUEST_FILENAME} !-f
            RewriteRule ^ index.php [L]

            # Handle Authorization Header
            RewriteCond %{HTTP:Authorization} .
            RewriteRule .* - [E=HTTP_AUTHORIZATION:%{HTTP:Authorization}]
            Order allow,deny
            Allow from all
        </Directory>
        </VirtualHost>


III. Test

You can test this project via this online link : http://demo.shipplus.com.vn/


Thanks