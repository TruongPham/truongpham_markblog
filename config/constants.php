<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Constants use for app
    |--------------------------------------------------------------------------
    |
    | Here you may specify which of the database connections below you wish
    | to use as your default connection for all database work. Of course
    | you may use many connections at once using the Database library.
    |
    */
    'BLOG_STATUS' => [
        'VERIFIED'      => 1,
        'PENDING'       => 2,
        'DISABLED'      => 4
    ],
    
    'ENTRY_TITLE_LENGTH' => 200,
    
    'USER_ROLE' => [
        'ADMIN'     => 1,
        'NORMAL'    => 0
    ],
];